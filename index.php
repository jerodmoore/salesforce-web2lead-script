<!--/*
Plugin Name: eDoc Web2Lead
Plugin URI: https://edocintelligence.com/
Description: Easy to use web2lead capture inferface for salesforce with Captcha.
Author: eDoc Intelligence LLC
Version: 1.02
Author URI: https://edocintelligence.com/
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/

You can contact us at info@edocintelligence.com

-->
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-type">
    <title>Web2Lead Salesforce submission form</title>
    <style></style>
  </head>
  <body>
    <h3>
      Great to hear from you!
    </h3>
    <h4>
      Tell us how to reach you and your area of interest and we will get back
      to you within 24 hours (or less).
    </h4>
    <table cellspacing="5" cellpadding="15" border="0">
      <tbody>
        <tr valign="top">
          <td>
            <p>
              <strong>Headquarters</strong><br>
              123 Streetname Blvd.<br>
              Suite 1<br>
              Austin, TX 78701
            </p>
            <p>
              Phone: <strong>522-555-5555</strong>
            </p>
            <p>
              Fax: 555-555-5555
            </p>
          </td>
          <td>
            <form action="test.php" method="post" name="emailForm" id="test" style="padding: 0px; margin: 0px;">
            <input type="hidden" value="00D80000000L3KH" name="oid">
            <input type="hidden" value="http://www.UPDATETHISTOTRACKINGLANDINGPAGE.com/?src=thankyou" name="retURL">


				<table cellspacing="5" cellpadding="5" border="0" align="center">
                <tbody>
                  <tr valign="top">
                    <td>
                      <label for="first_name">First Name</label>
                    </td>
                    <td>
                      <input type="text" size="20" name="first_name" maxlength=
                      "40" id="first_name">
                    </td>
                    <td>
                      <label for="last_name">Last Name</label>
                    </td>
                    <td>
                      <input type="text" size="20" name="last_name" maxlength=
                      "80" id="last_name">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr valign="top">
                    <td>
                      <label for="email">Email</label>
                    </td>
                    <td>
                      <input type="text" size="20" name="email" maxlength="80"
                      id="email">
                    </td>
                    <td>
                      <label for="phone">Phone</label>
                    </td>
                    <td>
                      <input type="text" size="20" name="phone" maxlength="40"
                      id="phone"><br>
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr valign="top">
                    <td>
                      <label for="company">Company</label>
                    </td>
                    <td>
                      <input type="text" size="20" name="company" maxlength=
                      "40" id="company">
                    </td>
                    <td>
                      <label for="city">City</label>
                    </td>
                    <td>
                      <input type="text" size="20" name="city" maxlength="40"
                      id="city">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td>
                      <label for="state">State/Province</label>
                    </td>
                    <td>
                      <input type="text" size="20" name="state" maxlength="20"
                      id="state">
                    </td>
                    <td>
                      <label for="country">Country</label>
                    </td>
                    <td>
                      <input type="text" size="20" name="country" maxlength=
                      "40" id="country"><br>
                    </td>
                  </tr>
                  <tr valign="top">
                    <td>
                      Area of Interest
                    </td>
                    <td>
                      <select title="Product__c" name="Product__c" id=
                      "Product__c">
                        <option>
                          --None--
                        </option>
                        <option value="area1">
                          Area1
                        </option>
                        <option value="Area2">
                          Area2
                        </option>
                        <option value="Area3">
                          Area3
                        </option>
                        <option value="Area4">
                          Area4
                        </option>
                        <option value="Area5">
                          Area5
                        </option>
                        <option value="Area6">
                          Area6
                        </option>
                        <option value="Area7">
                          Area7
                        </option>
                        <option value="Area8">
                          Area8
                        </option>
                        <option value="Area9">
                          Area9
                        </option>
                        <option value="Area10">
                          Area10
                        </option>
                      </select>
                    </td>
                    <td>
                      Comments
                    </td>
                    <td>
                      <textarea wrap="soft" type="text" rows="3" name=
                      "Comments__c" id="Comments__c" title="Comments__c">
</textarea>
                    </td>
                  </tr>
                  <tr valign="top" align="center">
                    <td></td>
                    <td></td>
                    <td></td>
                    <tr>
						  <td align="left">
							<span class="style1 style2">Verify Code:<br>
							<br>
							<!-- Captcha//-->
								<?php
								require_once('recaptcha/recaptchalib.php');
								$publickey = ""; // you got this from the signup page
								echo recaptcha_get_html($publickey);
								?>
							 <!-- Captcha //--></span>
						  </td>
					</tr>
                  </tr>
                  <tr valign="top">
                    <td>
						<br>
						<br>
						<input type="submit" name="submit"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
				</table>
	 </body>
</html>