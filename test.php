<?php
/*
Plugin Name: eDoc Web2Lead
Plugin URI: https://edocintelligence.com/
Description: Easy to use web2lead capture inferface for salesforce with Captcha.
Author: eDoc Intelligence LLC
Version: 1.02
Author URI: https://edocintelligence.com/
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/

You can contact us at info@edocintelligence.com

*/

ob_start();
session_start();
 
include_once('recaptcha/recaptchalib.php');
 
         //Recaptcha Settings
       $publickey = ""; // you got this from the signup page
	   $privatekey = "";
 
 
	//curl method posting
	//extract data from the post
      extract($_POST);
 
		if ($submit){
 
		$ok = 1;
 
		 $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);
 
		 if (!$resp->is_valid) {
			  $ok = 0;
			}
 
	if ($ok){		
		//set POST variables
		$url = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
		$fields = array(
					'oid'=>urlencode($oid),
					'company'=>urlencode($company),
					'state'=>urlencode($state),
					'recordType'=>urlencode($recordType),
					'retURL'=>urlencode($retURL),
					'first_name'=>urlencode($first_name),
					'last_name'=>urlencode($last_name),
					'email'=>urlencode($email),
					'phone'=>urlencode($phone),
					'city'=>urlencode($city),
					'country'=>urlencode($country),
					'Comments__c'=>urlencode($Comments__c),
					'Product__c'=>urlencode($Product__c)
				);
 
		//url-ify the data for the POST
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string,'&');
 
		//print_r($fields_string);
 
		//open connection
		$ch = curl_init();
 
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
 
		//execute post
		$result = curl_exec($ch);
 
		//close connection
		curl_close($ch);
 
	} //if ok
	else {
			  echo "<h4>Sorry - Invalid, That was not the correct captcha. Try Again Please.</h4>";
			}	
 
 
 } //if submit.
 ?>